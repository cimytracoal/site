//Coordinates from https://epsg.io/
var hPolygon={
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {},
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              -73.7097481,
              6.0520421
            ],
            [
              -73.7223942,
              6.0520505
            ],
            [
              -73.722406,
              6.0339655
            ],
            [
              -73.7097603,
              6.0339571
            ]
          ]
        ]
      }
    }
  ]
}